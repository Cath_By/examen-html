
from django.contrib import admin
from django.urls import path,include
from django.conf.urls import url, include
from rest_framework import routers
from MyList import views

router = routers.DefaultRouter()
router.register(r'persona', views.PersonaViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    url('',include('social.apps.django_app.urls',namespace='social')),
    path('',include('MyList.urls')),
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^oauth/', include('social_django.urls', namespace='social')),
    
]
