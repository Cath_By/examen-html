from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
from .models import Lista,Persona,Tienda
from datetime import datetime
# importar user
from django.contrib.auth.models import User
# sistema de autenticación
from django.contrib.auth import authenticate, logout, login as auth_login
from django.contrib.auth.decorators import login_required
from MyList.API.serializer import PersonaSerializer
from rest_framework import viewsets
from django.contrib.auth import logout

def logOut(request):
   logout(request)
   return redirect('/')


def index(request):
    usuario = request.session.get('usuario', None)
    return render(request, 'index.html', {'personas': Persona.objects.all(), 'usuario': usuario})


def registro(request):
    return render(request, 'registro.html', {})


def aplicacion(request):
    return render(request, 'App.html', {})


def listas(request):
    return render(request, 'listas.html', {})


def tiendas(request):
    return render(request, 'tiendas.html', {})


def crear(request):
    correo = request.POST.get('correo', '')
    nombre = request.POST.get('nombre', '')
    contrasenia = request.POST.get('contrasenia', '')
    persona = Persona(correo=correo, nombre=nombre, contrasenia=contrasenia)
    print(str(persona))
    persona.save()
    # [31/Oct/2018 16:55:27]
    # print(str.split(nacimiento))
    return redirect('registro')


def cerrar_session(request):
    del request.session['usuario']
    return redirect('index')


def entrar(request):
    return render(request, 'aplicacion.html', {})


def entrar_iniciar(request):
    nombre = request.POST.get('nombre_usuario', '')
    contrasenia = request.POST.get('contrasenia', '')

    user = authenticate(username=nombre, password=contrasenia)

    if user is not None:
        request.session['usuario'] = user.first_name+" "+user.last_name
        return redirect("aplicacion")
    else:
        return redirect("index")

class PersonaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Persona.objects.all().order_by('nombre')
    serializer_class = PersonaSerializer

def crear_lista(request):
    nombre_lista = request.POST.get('nombre_lista','')
    nombre_producto = request.POST.get('nombre_producto','')
    prosupuesto = request.POST.get('costo_presupuestado','')
    costo_real = request.POST.get('costo_real','')
    notas = request.POST.get('notas','')
    estado_producto = request.POST.get('estado_producto','')
    total_agregados = request.POST.get('total_agregados','')
    total_comprados = request.POST.get('total_comprados','')
    lista = Lista(nombre_lista=nombre_lista, nombre_producto=nombre_producto, prosupuesto=prosupuesto,costo_real=costo_real,notas=notas,estado_producto=estado_producto,total_agregados=total_agregados,total_comprados=total_comprados)
    print(str(lista))
    lista.save()
    return redirect('listas')

def crear_tienda(request):
    nombre_tienda = request.POST.get('nombre_tienda','')
    nombre_sucursal = request.POST.get('nombre_sucursal','')
    direccion = request.POST.get('direccion','')
    comuna = request.POST.get('ciudad','')
    región = request.POST.get('region','')
    tienda = Tienda(nombre_tienda=nombre_tienda,nombre_sucursal=nombre_sucursal,direccion=direccion,ciudad=ciudad,región=región)
    print(str(tienda))
    tienda.save()
