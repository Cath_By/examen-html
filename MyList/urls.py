from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from django.urls import path
from django.urls import reverse_lazy
from . import views
from django.conf.urls import url
from django.conf.urls import url, include
from rest_framework import routers
from django.contrib.auth import logout


urlpatterns = [
    path('',views.index, name='index'),
    path('registro/',views.registro,name='registro'),
    path('aplicacion/',views.aplicacion,name='aplicacion'),
    path('listas/',views.listas,name='listas'),
    path('listas/crear',views.listas,name='crear_lista'),
    path('tiendas/',views.tiendas,name='tiendas'),
    path('registro/crear',views.crear,name='crear'),
    path('entrar',views.entrar,name="entrar"),
    path('cerrar_session',views.cerrar_session,name="cerrar_session"),
    path('entrar/iniciar',views.entrar_iniciar,name="iniciar"),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^salir/',views.logOut, name='logOut'),
    

]