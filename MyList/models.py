from django.db import models
from django.utils import timezone

class Persona(models.Model):
    correo = models.EmailField(max_length=100)
    nombre = models.CharField(max_length=100)
    contrasenia = models.CharField(max_length=100)
    def __str__(self):
        return "PERSONA"
class Lista(models.Model):
    nombre_lista = models.CharField(max_length=100)
    nombre_producto = models.CharField(max_length=100)
    presupuesto = models.IntegerField()
    costo_real = models.IntegerField()
    notas = models.CharField(max_length=10000)
    estado_producto = models.CharField(max_length=100)
    total_agregados = models.IntegerField()
    total_comprados = models.IntegerField()
    def __str__(self):
        return "LISTA"

class Tienda(models.Model):
    nombre_tienda = models.CharField(max_length=100)
    nombre_sucursal = models.CharField(max_length=100)
    dirección = models.CharField(max_length=10000)
    comiuna = models.CharField(max_length=10000)
    región = models.CharField(max_length=10000)
    def __str__(self):
        return "TIENDA"



    

