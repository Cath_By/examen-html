from MyList.models import Persona
from rest_framework import serializers

class PersonaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Persona
        fields = ('correo','nombre','contrasenia')

