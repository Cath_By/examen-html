from MyList.models import Persona
from rest_framework import serializers,viewsets
from serializer import PersonaSerializer


class PersonaViewSet(viewsets.ModelViewSet):
    queryset = Persona.objects.all().order_by('nombre')
    serializer_class = PersonaSerializer
